# Inspirations from books
Hannes marks interesting stuff he reads in books and blogs and publishes them here.

## How Google Works
Eric Schmidt
Read July 2018

### Communications—Be a Damn Good Router

Write a review of your own performance, then read it and see if you would.

### Email wisdom

- Respond quickly.
- When writing an email, every word matters,
- Clean out your inbox constantly.
- Handle email in LIFO order

### Have a playbook

If managing partnerships is like diplomacy, then it stands to reason that it needs to be conducted by diplomats.

### Relationships, not hierarchy

- Three-week rule: When you start a new position, for the first three weeks don’t do anything. Listen!
- New recruits: first 3 weeks „pair following“ everyone
- And don’t forget to make people smile.


## Work Rules!

Insights from Inside Google That Will Transform How You Live and Lead

Bock, Laszlo
Read July 2018

### Preface: A Guidance Counselor’s Nightmare

Jack Welch, GE’s chairman and CEO from 1981 to 2001. He spent more than 50 percent of his time on people issues.

### Why Google’s Rules Will Work for You

Leading with your heart can make a successful business.

Manager should focus not on punishments or rewards but on clearing roadblocks and inspiring her team.

‘You make me want to be a better man’? That is how I feel about Terri as a manager.

All it takes is a belief that people are fundamentally good.

### 2. “Culture Eats Strategy for Breakfast”

If you believe people are good, you must be unafraid to share information with them Transparency is the second cornerstone of our culture. 

Every meeting is recorded and made available to all employees.

Culture matters most when it is tested.

### 3. Lake Wobegon, Where All the New Hires Are Above Average

When hiring people are extreemly biased: Assessment occurs in the first three to five minutes of an interview.

Only hire people who are better than you.

It takes longer to find these exceptional people, but it’s always worth the wait.

### 5. Don’t Trust Your Gut

Research:

Unstructured interviews have an r2 of 0.14 (Predicting 14% of future performance).

Reference checks 7%

The best predictor of how someone will perform in a job is a work sample test (29 percent).

The second-best predictors of performance are tests of general cognitive ability (26 percent).

Tied with tests of general cognitive ability are structured interviews (26 percent),

Cognitive ability (predicts 26 percent of performance), when combined with an assessment of conscientiousness (10 percent), is better able to predict who will be successful in a job (36 percent).

My experience is that people who score high on conscientiousness “work to completion”—meaning they don’t stop until a job is done rather than quitting at good enough—and are more likely to feel responsibility for their teams and the environment around them.

Best way: Combining behavioral and situational structured interviews with assessments of cognitive ability, conscientiousness, and leadership.

### 6. Let the Inmates Run the Asylum

Take power from your managers and trust your people to run things.

### 7. Why Everyone Hates Performance Management, and What We Decided to Do About It

We prefer meetings where managers sit together and review people as a group.

### 8. The Two Tails

We could reduce good management to a checklist.

### 9. Building a Learning Institution

Deliberate practice: intentional repetitions of similar, small tasks with immediate feedback, correction, and experimentation.

In your organization there are people who are expert on every facet of your work.

Individual performance scales linearly, while teaching scales geometrically.

### 10. Pay Unfairly

The awards pay out in the form of Google Stock Units (GSUs) that vest over time. 
Team members receive awards based on their level of involvement and contribution, 
and the largest awards to individuals can reach several million dollars.

### 11. The Best Things in Life Are Free (or Almost Free)

Our benefits and also our environment to increase the number of “moments of serendipity” that spark creativity.

We also decided to continue paying 50 percent of the Googler’s salary to the survivor for the next ten years. 

And if there were children, the family would receive an additional $ 1,000 each month until they turned nineteen, 
or twenty-three if they were full-time students.

### 12. Nudge … a Lot

Have a role-and-responsibilities discussion. Match your Noogler with a peer buddy. 
Help your Noogler build a social network. Set up onboarding check-ins once a month for your Noogler’s first six months. 
Encourage open dialogue.

It turns out checklists really do work, even when the list is almost patronizingly simple.

Research that shows the simple act of making decisions degrades one’s ability to make further decisions.

Ask questions, lots of questions Schedule regular 1: 1s with your manager. Get to know your team. 
Actively solicit feedback-don’t wait for it Accept the challenge (i.e., take risks and don’t be afraid to fail … other Googlers will support you).

Using nudges to help people become healthy, wealthy, and wise:

### 13. It’s Not All Rainbows and Unicorns

Googlers were very upset at the change, because even after just a few years they felt entitled to this program.

### 14. What You Can Do Starting Tomorrow

Don’t find the best salesperson; find the person who sells best to new accounts of a certain size. 
Find the person who excels at hitting golf balls at night in the rain. 
The more specific you can be in slicing expertise, the easier it will be to study your stars and discern why they are more successful than others.
Afterword for HR Geeks Only: Building the World’s First People Operations Team

While our best engineers may have hundreds of times more impact than an average engineer, 
our best recruiter doesn’t have hundreds of times more impact than average.

CEOs want a business orientation and analytical skill set that is harder to find in HR than it ought to be.



## Measure What Matters: OKRs: The Simple Idea that Drives 10x Growth
Doerr, John
Read July 2018

### Part One: OKRs IN ACTION

#### Google, Meet OKRs

Many companies have a “rule of seven,” limiting managers to a maximum of seven direct reports.

Google ranked number one on Fortune magazine’s list of “Best Companies to Work For.”

Managers become coaches, mentors, and architects. Actions—and data—speak louder than words.

#### The Father of OKRs

Many people work so hard and achieving so little.

The best way to solve a management problem, he believed, was through “creative confrontation”—by facing people “bluntly, directly, and unapologetically.” Despite Andy’s (Intel CEO) hot temper, he was down-to-earth and approachable, open to any good idea.

Hannes: I believe so too.

#### Superpower #1: Focus and Commit to Priorities

Values cannot be transmitted by memo,

“When you are tired of saying it, people are starting to hear it.”

OKRs are inherently works in progress, not commandments chiseled in stone.

Key results should be succinct, specific, and measurable. A mix of outputs and inputs is helpful.

#### Commit: The Nuna Story

Until your executives are fully on board, you can’t expect contributors to follow suit.

Spending hours cascading goals up and down the company, however, does not (help)

#### Superpower #2: Align and Connect for Teamwork

Top-down and bottom-up goals generally settles at around half-and-half.

#### Superpower #3: Track for Accountability

“We do not learn from experience … we learn from reflecting on experience.”

Hannes: Oh, I love that.

#### Superpower #4: Stretch for Amazing
For the rest, “stretched” goals could elicit maximum output.

### Part Two: THE NEW WORLD OF WORK

#### Continuous Performance Management

What business leaders have learned, very painfully, is that individuals cannot be reduced to numbers.

That transformational system, the contemporary alternative to annual reviews, is continuous performance management.

On the other hand, if you don’t have goals, what the heck are you talking about? What did you achieve, and how? In my experience, people are more likely to feel fulfilled when they have clear and aligned targets. They’re not wandering and wondering about their work; they can see how it connects and helps the organization.

It consists of four elements: 

- The first is a set of monthly one-on-one conversations between employees and their managers about how things are going. 
- The second is a quarterly review of progress against our OKRs. We sit down and say, ‘What did you set out to accomplish this quarter? What were you able to do—and what weren’t you able to do? Why or why not? What can we change?’ 
- Third, we have a semiannual professional development conversation. Employees talk about their career trajectory—where they’ve been, where they are, where they want to go. And how their managers and the organization can support their new direction. 
- The fourth bit is ongoing, self-driven insight.

`Hannes: We are doing exactly this. Nice.`

It centers on five questions: 

- What are you working on?
- How are you doing; how are your OKRs coming along? 
- Is there anything impeding your work? 
- What do you need from me to be (more) successful?
- How do you need to grow to achieve your career goals?

Andy Grove estimated that ninety minutes of a manager’s time “can enhance the quality of your subordinate’s work for two weeks.”

`@leads: Shall we introduce monthly or weekly 1:1 sessions?`

It should be regarded as the subordinate’s meeting, with its agenda and tone set by him …. The supervisor is there to learn and coach.

Based on BetterWorks’ experience with hundreds of enterprises, five critical areas have emerged of conversation between manager and contributor: Goal setting and reflection, where the employee’s OKR plan is set for the coming cycle. The discussion focuses on how best to align individual objectives and key results with organizational priorities. Ongoing progress updates, the brief and data-driven check-ins on the employee’s real-time progress, with problem solving as needed.fn3 Two-way coaching, to help contributors reach their potential and managers do a better job. Career growth, to develop skills, identify growth opportunities, and expand employees’ vision of their future at the company. Lightweight performance reviews, a feedback mechanism to gather inputs and summarize what the employee has accomplished since the last meeting, in the context of the organization’s needs.


Managers are evolving from taskmasters to teachers, coaches, and mentors.

Unlike negative criticism, coaching trains its sights on future improvement.

`Hannes: This is brilliant`

Continuous recognition is a powerful driver of engagement: “As soft as it seems, saying ‘thank you’ is an extraordinary tool to building an engaged team

`Hannes: this was my idea behind #thanks`

Alex: Every two weeks, each person at Zume has a one-hour, one-on-one conversation with whomever they report to.

It’s a sacred time. You cannot be late; you cannot cancel.

An internal Google study of 180 teams, standout performance correlated to affirmative responses to these five questions: 

1. Structure and clarity: Are goals, roles, and execution plans on our team clear? 
2. Psychological safety: Can we take risks on this team without feeling insecure or embarrassed? 
3. Meaning of work: Are we working on something that is personally important for each of us? 4. Dependability: Can we count on each other to do high-quality work on time? 
4. Impact of work: Do we fundamentally believe that the work we’re doing matters?

`@leads: Let's review those`

#### Culture

You do it because every OKR is transparently important to the company, and to the colleagues who count on you. Nobody wants to be seen as the one holding back the team. Everybody takes pride in moving progress forward. It’s a social contract, but a self-governed one.

These organizations don’t merely engage their workers. They inspire them. They replace rules with shared principles; carrots and sticks are supplanted by a common sense of purpose.

#### Culture Change: The Lumeris Story

OBJECTIVE

Institute a culture that attracts and retains A players. 

KEY RESULTS 

1. Focus on hiring A player managers/ leaders. 
2. Optimize recruitment function to attract A player talent. 
3. Scrub all job descriptions
4. Retrain everyone engaged in the interviewing process.
5. Ensure ongoing mentoring/ coaching opportunities. 
6. Create a culture of learning for development of new and existing employees.

He set the tone for what he calls “brutal transparency without judgment.” 

### Resource 1: Google’s OKR Playbook

Objectives are the “Whats.” They: 

- express goals and intents; 
- are aggressive yet realistic; 
- must be tangible, objective, and unambiguous; should be obvious to a rational observer whether an objective has been achieved. 
- The successful achievement of an objective must provide clear value for Google. Key Results are the “Hows.” They: 
- express measurable milestones which, if achieved, will advance objective( s) in a useful manner to their constituents; 
- must describe outcomes, not activities. If your KRs include words like “consult,” “help,” “analyze,” or “participate,” they describe activities. Instead, describe the end-user impact of these activities: “publish average and tail latency measurements from six Colossus cells by March 7,” rather than “assess Colossus latency”; 
- must include evidence of completion. This evidence must be available, credible, and easily discoverable. Examples of evidence include change lists, links to docs, notes, and published metrics reports.

#### Classic OKR-Writing Mistakes and Traps

`@leads: let’s review those at the beginning of our session`

### Resource 2: A Typical OKR Cycle

A Typical OKR Cycle

`@leads: We will use this`

#### Resource 3: All Talk: Performance Conversations

Manager-led Coaching To prepare for this conversation, the manager should consider the following questions: 

- What behaviors or values do I want my report to continue to exhibit? 
- What behaviors or values do I want the report to start or stop exhibiting? 
- What coaching can I provide to help the report fully realize his or her potential? 
- During the conversation, the leader might ask: What part of your job most excites you? What (if any) aspect of your role would you like to change?

Upward Feedback To elicit candid input from a contributor, the manager might ask: 

- What are you getting from me that you find helpful? 
- What are you getting from me that impedes your ability to be effective? 
- What could I do for you that would help you to be more successful?

### Resource 5: For Further Reading

#### For Further Reading Andy Grove and Intel 

- High Output Management, by Andrew S. Grove 
- Andy Grove: The Life and Times of an American, by Richard S. Tedlow 
- The Intel Trinity: How Robert Noyce, Gordon Moore, and Andy Grove Built the World’s Most Important Company, by Michael Malone 

#### Culture 
 
- HOW: Why HOW We Do Anything Means Everything, by Dov Seidman 
- Lean In: Women, Work, and the Will to Lead, by Sheryl Sandberg 
- Radical Candor: Be a Kick-Ass Boss Without Losing Your Humanity, by Kim Scott Jim Collins 
- Good to Great: Why Some Companies Make the Leap … and Others Don’t 
- Great by Choice: Uncertainty, Chaos, and Luck—Why Some Thrive Despite Them All 

#### Bill Campbell and Coaching 

- Playbook: The Coach—Lessons Learned from Bill Campbell, forthcoming book by Eric Schmidt, Jonathan Rosenberg, and Alan Eagle 
- Straight Talk for Startups: 100 Insider Rules for Beating the Odds, by Randy Komisar Google 
- How Google Works, by Eric Schmidt and Jonathan Rosenberg 
- Work Rules!: Insights from Inside Google That Will Transform How You Live and Lead, by Laszlo Bock 
- In the Plex: How Google Thinks, Works, and Shapes Our Lives, by Steven Levy 

#### OKRs 
- www.whatmatters.com 
- Radical Focus: Achieving Your Most Important Goals with Objectives and Key Results, by Christina Wodtke


## Gitlab Handbook
about.gitlab.com/handbook
Read August 2018

### General Guidelines
#### Values

We create simple software to accomplish complex collaborative tasks.

We want to solve problems while they are small. We want to have a great company

There is no need for consensus.

Everyone at the company cares about your output. Being away from the keyboard during the workday, doing private browsing or making personal phone calls is fine and encouraged.

Nothing will ever be finished.

Respect the privacy of our users and your fellow team members. Secure our production data at all times.

Looking into production data for malicious reasons is a fireable offense. 

Be gentle when reminding people about these guidelines,

### Internal Communication

All written communication happens in English.

Hannes: I would go a step further: All communication

Use asynchronous communication when possible (issues and email instead of chat).

Having pets, children, significant others, friends, and family visible during video chats is encouraged. If they are human, ask them to wave at your remote team member to say "Hi".

Hannes: Oh, I love that so much.

### GitLab Workflow

If a merge request fixes a UX issue, it should be assigned to a UX Designer for review. 


### Release Retrospectives and Kickoffs

After GitLab releases a new version on the 22nd of each month, we have a 30-minute call on the next business day reflecting

@Leads: Retro after each sprint?

### Spending Company Money

Motto: Results, freedom, efficiency, frugality, and boring solutions,

### Sales Target Dinner Evangelism Reward

We reward all team members (not just the Sales team) for hitting a sales goal because it is a team effort.

Hannes: I love that.

### Discretionary Bonuses

Bonus; and the "why" should be tied to our values.

### Leadership

Start meetings on time, be on time yourself, don't ask if everyone is there, and don't punish people that have shown up on time by waiting for anyone or repeating things for people that come late.

@team: I want this too. Start meetings on the dot! We need to be more considerate with other peoples time.

If you meet external people always ask what they think we should improve.

@pms: That is a good point. We should introduce quarterly retros with our clients to get feedback and improve.

Strive to make the organization simpler.

We don't have project managers.

@pms: 1. Should we call our pms "customer success" managers or "client reps"?
Should we pull the "pms" out of the coding spring and only have them work on the backlog?

### Management

People should not be given a raise or a title because they ask for it or threaten to quit. We should proactively give raises.

Families come together for the relationship and do what is critical to retain it. Teams are assembled for the task and do what is required to complete it.

Hannes: That hit me hard... I have been thinking about this for a week. Here is my take on it. "Having a job" = "people work to achieve a goal". Families exist for the people, most companies exist for the goal, we exist for the work. 

stanwood: "The journay is the reward" as we say way more elegantly in German.

### Marketing team SLAs (Service Level Agreements)

Do not ping someone in a public channel on chat on the weekends. This is rude.

@hannes: I agree. We should all disable notifications by default on weekends.

### Anatomy of a Blog Post

Anatomy of a Blog Post
@karin: Check this out. It's brilliant.


### Checklist for Onboarding New BDR Hires
@richard: Check this. It's brilliant.

### Security Handbook
@martin: Check this out. I love it.

### Phishing Tests

GitLab conducts routine phishing tests using a third-party testing platform. All team members will occasionally receive emails that are designed to look like legitimate business-related communications but will in actuality be simulated phishing attacks.

@martin: Do we need this?


## Zapier: The Ultimate Guide to Remote Work
https://zapier.com/learn/remote-work/
Read August 2018

### How to Run a Remote Team

We’ve found there are three important ingredients to making remote work, well, work: Team, Tools, and Process.

Hire people who can write.

@leads: Should we test somehow for people who can write a compact and well structured 140 character agreement?

"Pair Buddies"

@leads: Every Wooky should get a buddy from another team.

Read this:

- Ryan Carson Mixergy Course Almost 
- Any talk by Zach Holman on how GitHub works 
- Wide Teams puts out a great weekly podcast about remote work 
- Steve Smith on Optimizing for Happiness
- Adii Pienaar on trusting people
- Adii Pienaar on the challenge of remote working

### How to Build Culture in a Remote Team

Slack is that our water cooler discussions are recorded. Nothing gets lost. And there’s no “behind-your-back politics” that happens in many co-located offices.


P2, or Async as well call it, is a WordPress theme that makes it really easy to post updates.

We do this by sharing weekly updates on our internal blog (P2) every Friday—I

### How to Hire a Remote Team

Proficient written communicator:

@hannes: Read up on buffer

People with these traits often come from freelance, contracting or startup backgrounds.

First fill the position yourself, even if it’s only for a week.

Almost every candidate mentions the blog as a reason.

DoSomething runs hiring with Trello.

@hannes: We could use Jira/gitlab for hiring...

### How Successful Remote Teams Evaluate Employees

On at Automattic was having a veteran employee as a “buddy” to help me navigate the uncharted

Two elements are crucial. First, the format (Start, Stop, and Continue) provides a framework that makes a difficult task (giving peer feedback) easier. The main purpose is to help employees organize their thoughts. Second, the feedback should be focused on behaviors, not personalities. The former is something an employee can improve; the latter isn’t.

His own opinions. Thoughts from co-workers. A self-evaluation from the actual employee. 

“We hold the opinion that you should share praise and own blame.” -Greg Ciotti, Content

@hannes: My definition of leadership: Always my fault, never my success.

### How to End Internal Emails and Communicate Effectively in a Remote Team

@hannes: Read:

- Day of Communication at GitHub
- Effective Meeting Tactics employed by Execs at LinkedIn, 

Amazon and Asana” Treehouse Constructed a Reddit Clone to Bubble Up Important Items


### How to Build Strong Relationships in a Remote Team

@hannes: Read:

- It’s Not All About Me: Ten Techniques for Building Quick Rapport With Anyone.

“True validation coupled with ego suspension means that you have no story to offer, that you are there simply to hear theirs.” 

We love to talk about ourselves, especially to good listeners, but this means we’re often ready to respond with our own related stories when our conversation partner finishes talking. According to Dreeke, ego suspension means putting aside our own desires to contribute to the conversation, and instead asking short, open questions like how, when, and why.

"We should always assume ignorance before malice."

@team: That is a very good mantra.

Before the weekly meeting, each teammate writes a short bit about what they are working on.

The first 10 minutes of our meeting is complete silence.

Each person gets 5 minutes to ask questions.

@pms: Lets do that for our weekly. Before the meeting we post our project status to #done. Then we read for 10 minutes in silence. Then every person can ask questions for 5 minutes.

### How to Run a Company Retreat for a Remote Team

There’s something special about cooking a meal for your teammates that.

hannes: Roger that!

Doing something physical is also a great way to learn more about each other. 

### How to Work in Different Timezones
GoToMeeting is still the best tool we’ve found



## How Doist Makes Remote Work Happen – Ambition & Balance

Doist
Read August 2018

### Doist’s values

Undergone at least one test task and at least three interviews with three different Doisters.

If it’s not a “Hell Yeah!”, it’s a "hell no".

hannes: That is actually my motto for hiring now.

An employee-centric, sustainable vision.

hannes: This is us as well.

It’s never acceptable to post an out of office notice to the team that includes any type of phrasing like “I’ll be mostly offline, but if needed you can reach me at xxx.”

Mandatory five weeks of paid vacation per year in addition to national holidays in your country of residence. Mandatory eighteen weeks


## Toggle Guide to Remote Work
https://toggl.com/out-of-office/
Read August 2018

### Building

One of the main things that keeps a lot of remote teams together and sane is a sense of humor.

"If it's not hell yeah then it's hell no".

If it takes them a week to respond to your emails...

### Hiring

WeWorkRemotely, Remote.co, FlexJobs, and others focus exclusively on remote positions.

hannes: They get 1000-2000 applicants for every position that way.

For effective candidate pre-screening, we use our own skills-based hiring app: Hundred5.We

### Grow

Even if you’ve spoken to your new hire via video calls many times during the interviewing process, it’s important to do it again after you’ve closed the deal.


Employee onboarding, like many other processes in teams both traditional and remote, should be constantly changing and collaboratively improved on.

(say, a few weeks, and then again after a month), sit them down and ask them how they feel about their onboarding. 

### Make it work

CEO or another management position and team members, it’s also good to have some time with the entire team to talk about whatever comes to mind—basically

### How to lead

“I’m a firm believer in trust but verify. Each team and position should have some trackable metrics that indicate the level of productivity achieved.

## Who: The A Method for Hiring

Smart, Geoff; Street, Randy

Read April 2019

### Introduction

Who is your number-one problem. Not what.

A Single hiring blunder on a $ 100,000 employee can cost a company $ 1.5 million or more.

### CHAPTER ONE

“Managing is easy, except for the people part!” ;-))))

Scorecards succeed because they focus on outcomes, or what a person must get done (grow revenue from $ 25 million to $ 50 million by the end of year three). 

Ironically, all that specificity frees new hires to give the job their best shot.

Hannes: Let's put the okrs on the job offer and focus on them during hiring.


Critical Competencies for A Players

- Efficiency: Able to produce significant output with minimal wasted effort.
- Honesty/ integrity. Does not cut corners ethically. Earns trust and maintains confidences.

Common interview processes are “almost a random predictor” of job performance.

A Players tend to talk about outcomes linked to expectations. B and C Players talk generally about events, people they met, or aspects of the job they liked.

What people say, that were fired:

“It was mutual.” “It was time for me to leave.” “My boss and I were not getting along.” “Judy got promoted and I did not.” “My role shrank.” “I missed my number and was told I was on thin ice.” “I slapped the CEO so hard that I lost my $ 3 million severance package.” 

What people say who quit: 

“My biggest client hired me.” “My old boss recruited me to a bigger job.” “The CEO asked me

What Got You Here Won’t Get You There, Goldsmith identifies twenty behavioral derailers that can hurt an executive’s career.

### CHAPTER FIVE

Five F’s of selling, are: fit, family, freedom, fortune, and fun.

### CHAPTER SIX

Boards and investors have a tendency to invest in CEOs who demonstrate openness to feedback, possess great listening skills, and treat people with respect.

"Lambs" were successful 57 percent of the time.

"Cheetahs" in our study were successful 100 percent of the time.

“I want you to pause for a moment and think about the very best person you have working for you. Now I want you to think about the second-best person you have working for you. Now I would like you to think about where your organization would be without them. You would be terrified if you lost them. And you would love to have ten more like them. That is how I feel about the importance of hiring, promoting.