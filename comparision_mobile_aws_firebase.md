UPDATE: Migrated and moved to https://blog.stanwood.io/insights/comparison-aws-vs.-firebase

# Comparison AWS vs. Firebase
Created August 20th, 2018

We compare the mobile backend services that Amazon Web Services (AWS) and Google Cloud Platform (GCP) provide in their respective cloud environments.

## Comparision

### Same
| Feature | AWS Mobile Hub | Firebase |
|:--|:--|:--|
| Auth | Yes | Yes |
| Storage/CDN | Yes | Yes |
| Database | Yes | Real time |
| Serverless routines | Yes | Yes |
| Push notifications | Yes | Free |
| Analytics | Yes | Yes |

### Firebase strengths
| Feature | AWS Mobile Hub | Firebase |
|:--|:--|:--|
| Offline | Some | Yes |
| In App Messaging | No | Yes |
| Crash reporting | No | Yes |
| App indexing | No | Yes |
| Remote configs | No | Yes |
| A/B Testing | Some | Yes |
| Implementation | Slow | Fast |
| Cost | API calls, push + traffic | Traffic

### Recommendation
stanwood is running some 100+ apps on firebase at literally only the cost of traffic. Development is super fast - as you only need an SDK and no backend development. 

Our biggest apps have million of active users and over the last 3 years since we have been using firebase we were not experiencing a single outage.

Firebase also acquired crashlytics (the leading crash reporting and error tool from Twitter) and is building firebase analytics on the backbone of Google analytics with export to BigQuery.

Firebase is also heavily investing in features around A/B testing, machine learning (like predictive targeting) and CRM.

The UI is intuitive enough that we have non-devs working them to send push, change configs or run experiments.

The biggest downside of firebase with the SDK is, that they do not offer any logs, monitoring or out of the box error reporting. We usually opt for our own APIs for auth, data and storage running on Google App Engine.

## Links
https://firebase.google.com/products
https://aws.amazon.com/mobile/

## Detailed comparision
https://cloud.google.com/docs/compare/aws/mobile 